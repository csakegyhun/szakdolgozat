import * as express from 'express';
import * as typeorm from 'typeorm';
import router from './routes';
import { cors } from './middlewares'
import * as bodyParser from 'body-parser';
import { User } from './models'
import * as fs from 'fs';

export class App {

    private application: express.Application;

    constructor() {
        
        this.application = express();
        this.application.use(cors);
        this.application.use(bodyParser.json());
        this.application.use(router);
    }

    public run() {
        this.application.listen(4202, async () => {
            await typeorm.createConnection();
            console.log("Connection was successfull");

        });
    }
/*
    public async loadJobs() {
        const counter = await typeorm.getRepository(Jobs)
            .createQueryBuilder("jobs")
            .select("jobs.name")
            .getCount();

        if (counter == 0) {
            let rawdata = fs.readFile('../AllaskeresoNodeJs2/database/jobs.json', (err, data) => {
                let value = JSON.parse(data.toString());
                value.forEach(async element => {
                    await typeorm.getConnection()
                        .createQueryBuilder()
                        .insert()
                        .into(Jobs)
                        .values(element)
                        .execute();
                });
            });
        }
    }

    public async loadUsers() {
        const counter = await typeorm.getRepository(Users)
            .createQueryBuilder("users")
            .select("users.username")
            .getCount();

        if (counter == 0) {
            let rawdata = fs.readFile('../AllaskeresoNodeJs2/database/users.json', (err, data) => {
                let value = JSON.parse(data.toString());
                value.forEach(async element => {
                    await typeorm.getConnection()
                        .createQueryBuilder()
                        .insert()
                        .into(Users)
                        .values(element)
                        .execute();
                });
            });
            await this.loadContact();
        }
    }

    public async loadStatus() {
        const counter = await typeorm.getRepository(Status)
            .createQueryBuilder("status")
            .select("status.code")
            .getCount();

        if (counter == 0) {
            let rawdata = fs.readFile('../AllaskeresoNodeJs2/database/status.json', (err, data) => {
                let value = JSON.parse(data.toString());
                value.forEach(async element => {
                    await typeorm.getConnection()
                        .createQueryBuilder()
                        .insert()
                        .into(Status)
                        .values(element)
                        .execute();
                });
            });
        }
    }

    public async loadSalary() {
        const counter = await typeorm.getRepository(Salary)
            .createQueryBuilder("salary")
            .select("salary.name")
            .getCount();

        if (counter == 0) {
            let rawdata = fs.readFile('../AllaskeresoNodeJs2/database/salary.json', (err, data) => {
                let value = JSON.parse(data.toString());
                value.forEach(async element => {
                    await typeorm.getConnection()
                        .createQueryBuilder()
                        .insert()
                        .into(Salary)
                        .values(element)
                        .execute();
                });
            });
            await this.loadUsers();
        }
    }

    public async loadFilter() {
        const counter = await typeorm.getRepository(Filter)
            .createQueryBuilder("filter")
            .select("filter.name")
            .getCount();

        if (counter == 0) {
            let rawdata = fs.readFile('../AllaskeresoNodeJs2/database/filter.json', (err, data) => {
                let value = JSON.parse(data.toString());
                value.forEach(async element => {
                    await typeorm.getConnection()
                        .createQueryBuilder()
                        .insert()
                        .into(Filter)
                        .values(element)
                        .execute();
                });
            });
        }
    }

    public async loadCountry() {
        const counter = await typeorm.getRepository(Country)
            .createQueryBuilder("country")
            .select("country.name")
            .getCount();

        if (counter == 0) {
            let rawdata = fs.readFile('../AllaskeresoNodeJs2/database/country.json', (err, data) => {
                let value = JSON.parse(data.toString());
                value.forEach(async element => {
                    await typeorm.getConnection()
                        .createQueryBuilder()
                        .insert()
                        .into(Country)
                        .values(element)
                        .execute();
                });
            });
        }
    }

    public async loadContact() {
        const counter = await typeorm.getRepository(Contact)
            .createQueryBuilder("contact")
            .select("contact.name")
            .getCount();

        if (counter == 0) {
            let rawdata = fs.readFile('../AllaskeresoNodeJs2/database/contact.json', (err, data) => {
                let value = JSON.parse(data.toString());
                value.forEach(async element => {
                    await typeorm.getConnection()
                        .createQueryBuilder()
                        .insert()
                        .into(Contact)
                        .values(element)
                        .execute();
                });
            });
            await this.loadJobs();
        }
    }

    public async loadCompany() {
        const counter = await typeorm.getRepository(Company)
            .createQueryBuilder("company")
            .select("company.name")
            .getCount();

        if (counter == 0) {
            let rawdata = fs.readFile('../AllaskeresoNodeJs2/database/company.json', (err, data) => {
                let value = JSON.parse(data.toString());
                value.forEach(async element => {
                    await typeorm.getConnection()
                        .createQueryBuilder()
                        .insert()
                        .into(Company)
                        .values(element)
                        .execute();
                });
            });
        }
    }

    public async loadCity() {
        const counter = await typeorm.getRepository(City)
            .createQueryBuilder("city")
            .select("city.name")
            .getCount();

        if (counter == 0) {
            let rawdata = fs.readFile('../AllaskeresoNodeJs2/database/city.json', (err, data) => {
                let value = JSON.parse(data.toString());
                value.forEach(async element => {
                    await typeorm.getConnection()
                        .createQueryBuilder()
                        .insert()
                        .into(City)
                        .values(element)
                        .execute();
                });
            });
        }
    }

    public async loadCompanyAudit(){
        const counter = await typeorm.getRepository(CompanyAudit)
            .createQueryBuilder("companyaudit")
            .select("companyaudit.name")
            .getCount();

        if (counter == 0) {
            let rawdata = fs.readFile('../AllaskeresoNodeJs2/database/companyAudit.json', (err, data) => {
                let value = JSON.parse(data.toString());
                value.forEach(async element => {
                    await typeorm.getConnection()
                        .createQueryBuilder()
                        .insert()
                        .into(CompanyAudit)
                        .values(element)
                        .execute();
                });
            });
        }
    }

    public async loadContactAudit(){
        const counter = await typeorm.getRepository(ContactAudit)
            .createQueryBuilder("contactaudit")
            .select("contactaudit.name")
            .getCount();

        if (counter == 0) {
            let rawdata = fs.readFile('../AllaskeresoNodeJs2/database/contactAudit.json', (err, data) => {
                let value = JSON.parse(data.toString());
                value.forEach(async element => {
                    await typeorm.getConnection()
                        .createQueryBuilder()
                        .insert()
                        .into(ContactAudit)
                        .values(element)
                        .execute();
                });
            });
        }
    }

    public async loadJobsAudit(){
        const counter = await typeorm.getRepository(JobsAudit)
            .createQueryBuilder("jobsaudit")
            .select("jobsaudit.name")
            .getCount();

        if (counter == 0) {
            let rawdata = fs.readFile('../AllaskeresoNodeJs2/database/jobsAudit.json', (err, data) => {
                let value = JSON.parse(data.toString());
                value.forEach(async element => {
                    await typeorm.getConnection()
                        .createQueryBuilder()
                        .insert()
                        .into(JobsAudit)
                        .values(element)
                        .execute();
                });
            });
        }
    }
*/
}