import { Request, Response } from 'express';
import { User, Product } from '../models';

export async function getAll(req: Request, res: Response) {
    res.json(await Product.find());
}

export async function getOne(req: Request, res: Response) {
    res.send(await Product.findOne({
        where: {
            id: req.params.id
        }
    }));
}

export async function edit(req: Request, res: Response) {
    delete req.body.action;

    const product = await Product.findOne({
        where: {
            id: req.body.id
        }
    })

    delete product.id;

    product.name = req.body.name;
    product.description = req.body.description;
    product.price = req.body.price;
    console.log(product);

    try {
        Product.update(req.body.id, product);
        console.log("Succesfull product edit");
        res.status(200).send();
    } catch (error) {
        res.status(400).send();
    }
}

export async function create(req: Request, res: Response) {
    const product = new Product();

    product.name = req.body.name;
    product.price = req.body.price;
    product.description = req.body.description;
    product.imagePath = req.body.imagePath;

    Product.save(product);
    console.log("Succesfull product create");
    res.status(200).send();
}

/*
export async function register(req: Request, res: Response) {
    const user = new User();

    console.log(req.body);

    user.username = req.body.username;
    user.email = req.body.email;
    user.password = req.body.password;
    user.status = await Status.findOne({
        where: {
            appellation: req.body.status
        }
    });

    User.save(user);
    console.log("Succesfull user save");
    res.status(200).send();
}

export async function edit(req: Request, res: Response) {
    delete req.body.action;

    const user = await User.findOne({
        where: {
            id: req.body.id
        }
    })

    delete user.id;

    user.username = req.body.username;
    user.email = req.body.email;
    user.password = req.body.password;
    user.status = await Status.findOne({
        where: {
            appellation: req.body.status
        }
    });
    console.log(user);

    try {
        User.update(req.body.id, user);
        console.log("Succesfull user edit");
        res.status(200).send();
    } catch (error) {
        res.status(400).send();
    }
}

export async function remove(req: Request, res: Response) {
    const deleteUser = await User.findOne({
        where: {
            id: req.params.id
        }
    });

    if (deleteUser == null) {
        res.status(404).send();
        return;
    }
    deleteUser.remove();
    console.log("Succesfull user delete");
    res.send();
}
*/