import { Request, Response } from 'express';
import { Role, User } from '../models';

export async function getAll(req: Request, res: Response) {
    res.json(await Role.find());
}