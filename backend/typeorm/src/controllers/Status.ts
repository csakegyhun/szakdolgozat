import { Request, Response } from 'express';
import { Status } from '../models';

export async function getAll(req: Request, res: Response) {
    res.json(await Status.find());
}

export async function register(req: Request, res: Response) {
    delete req.body.action;

    const status = new Status();
    status.code = req.body.code;
    status.appellation = req.body.appellation;
    await Status.save(status);
    console.log("Succesfull status save");
    res.status(200).send();
}

export async function edit(req: Request, res: Response) {
    delete req.body.action;
    const status = await Status.findOne({
        where: {
            id: req.body.id
        }
    })

    delete status.id;

    

    status.code = req.body.code;
    status.appellation = req.body.appellation;

    try {
        await Status.update(req.body.id, status);
        console.log("Succesfull status edit");
        res.status(200).send();
    } catch (error) {
        res.status(400).send();
    }
}

export async function remove(req: Request, res: Response) {
    const status = await Status.findOne({
        where: {
            id: req.params.id
        }
    });

    if (status == null) {
        res.status(404).send();
        return;
    }
    await status.remove();
    console.log("Succesfull status delete");
    res.send();
}