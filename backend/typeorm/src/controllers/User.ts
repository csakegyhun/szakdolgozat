import { Request, Response } from 'express';
import { Role, User } from '../models';

export async function getAll(req: Request, res: Response) {
    res.json(await User.find({
        relations: [
            "role"
        ]
    }
));
}

export async function authenticate(req: Request, res: Response) {
    res.json(await User.findOne({
        where: {
            username: req.body.username,
            password: req.body.password
        },
        relations: [
            "role"
        ]
    }));
}

export async function register(req: Request, res: Response) {
    const user = new User();

    user.username = req.body.username;
    user.email = req.body.email;
    user.password = req.body.password;
    user.registeredat = new Date();
    
    console.log(req.body);

    if(req.body.status != null){
        user.role = await Role.findOne({
            where: {
                name: req.body.status
            }
        });
    }else{
        user.role = user.role = await Role.findOne({
            where: {
                name: "User"
            }
        });
    }

    User.save(user);
    console.log("Succesfull user save");
    res.status(200).send();
}
/*
export async function edit(req: Request, res: Response) {
    delete req.body.action;

    const user = await User.findOne({
        where: {
            id: req.body.id
        }
    })

    delete user.id;

    user.username = req.body.username;
    user.email = req.body.email;
    user.password = req.body.password;
    user.status = await Status.findOne({
        where: {
            appellation: req.body.status
        }
    });
    console.log(user);

    try {
        User.update(req.body.id, user);
        console.log("Succesfull user edit");
        res.status(200).send();
    } catch (error) {
        res.status(400).send();
    }
}
*/
export async function remove(req: Request, res: Response) {
    const deleteUser = await User.findOne({
        where: {
            id: req.params.id
        }
    });

    if (deleteUser == null) {
        res.status(404).send();
        return;
    }
    deleteUser.remove();
    console.log("Succesfull user delete");
    res.send();
}
