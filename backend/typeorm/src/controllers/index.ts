import * as User from './User';
import * as Status from './Status';
import * as Product from './Product';
import * as Role from './Role';

export {
    User,
    Status,
    Product,
    Role
}