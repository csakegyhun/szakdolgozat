import * as express from 'express';
import * as application from './app';

const app = new application.App();
app.run();