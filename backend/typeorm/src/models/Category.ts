import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne, Timestamp, OneToOne, OneToMany } from 'typeorm';
import { Product } from '.';

@Entity()
export class Category extends BaseEntity
{

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    name: string;

    @OneToMany(() => Product, product => product.category)
    product: Product[];
}