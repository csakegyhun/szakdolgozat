import { OneToMany } from 'typeorm';
import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne, Timestamp, OneToOne } from 'typeorm';
import { Product } from '.';

@Entity()
export class Discount extends BaseEntity
{
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    name: string;

    @Column()
    description: string;

    @Column()
    percent: number;

    @Column()
    createdat: Date;

    @OneToMany(() => Product, product => product.discount)
    product: Product[];
}