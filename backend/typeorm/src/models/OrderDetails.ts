import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne, Timestamp, OneToOne } from 'typeorm';
import { User } from './User';
import { PaymentDetails } from './PaymentDetails';

@Entity()
export class OrderDetails extends BaseEntity
{

    @PrimaryGeneratedColumn()
    public id: number;

    @OneToOne(() => User, user => user.id)
    userId: number;

    @Column()
    total: number;

    @OneToOne(() => PaymentDetails, paymentDetails => paymentDetails.id)
    paymentId: PaymentDetails;

    @Column()
    createdAt: Date;
}