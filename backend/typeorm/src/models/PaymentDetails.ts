import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne, Timestamp, OneToOne } from 'typeorm';

@Entity()
export class PaymentDetails extends BaseEntity
{

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    orderId: number;

    @Column()
    amount: number;

    @Column()
    provider: string;

    @Column()
    status: string;

    @Column()
    createdAt: Date;
}