import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne, Timestamp, OneToOne } from 'typeorm';
import { Category } from './Category'
import { Discount } from './Discount'

@Entity()
export class Product extends BaseEntity
{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    description: string;

    @ManyToOne(() => Category, category => category.id)
    category: Category;

    @Column()
    price: number;

    @ManyToOne(() => Discount, discount => discount.product)
    discount: Discount;

    @Column()
    imagePath: string;
}