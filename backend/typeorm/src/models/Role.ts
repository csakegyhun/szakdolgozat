import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne, Timestamp, OneToOne, OneToMany } from 'typeorm';
import { User } from './User';

@Entity()
export class Role extends BaseEntity
{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    code: number;

    @OneToMany(() => User, user => user.role)
    user: User[];
}