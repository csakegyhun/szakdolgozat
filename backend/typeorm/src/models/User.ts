import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne, OneToOne } from 'typeorm';
import { Role } from './Role'

@Entity()
export class User extends BaseEntity
{
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    username: string;

    @Column()
    password: string;

    @Column()
    email: string;

    @Column()
    registeredat: Date;

    @ManyToOne(() => Role, role => role.id)
    role: Role;
}