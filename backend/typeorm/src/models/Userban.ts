import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne, Timestamp, OneToOne } from 'typeorm';
import { User } from './User';

@Entity()
export class Userban extends BaseEntity
{
    @PrimaryGeneratedColumn()
    public id: number;

    @OneToOne(() => User, user => user.id)
    banneduser: User;

    @Column()
    active: boolean;

    @Column()
    createdat: Date;

    @Column()
    description: string;

    @OneToOne(() => User, user => user.id)
    author: User;
}