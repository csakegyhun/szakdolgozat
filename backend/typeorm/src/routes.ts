import * as express from 'express';
import { User, Product, Role } from './controllers';


const router = express.Router();

router.use("/users", ((usersR) => {
    usersR.get('/', User.getAll);
    usersR.post('/', User.register);
    usersR.post('/authenticate', User.authenticate);
    //usersR.put('/:id', Users.edit);
    usersR.delete('/:id', User.remove);
    return usersR;
})(express.Router()));
router.use("", ((productsR) => {
    productsR.get('/products', Product.getAll);
    productsR.get('/product/:id', Product.getOne);
    productsR.post('/product', Product.create);
    //usersR.post('/authenticate', User.authenticate);
    productsR.put('/product/:id', Product.edit);
    //usersR.delete('/:id', Users.remove);
    return productsR;
})(express.Router()));

router.use("/", ((roleR) => {
    roleR.get('/roles', Role.getAll);
    // cityR.post('/', City.register);
    // cityR.put('/:id', City.edit);
    // cityR.delete('/:id', City.remove);
    return roleR;
})(express.Router()));
/*
router.use("/companies", ((companyR) => {
    companyR.get('/', Company.getAll);
    companyR.post('/', Company.register);
    companyR.put('/:id', Company.edit);
    companyR.delete('/:id', Company.remove);
    return companyR;
})(express.Router()));

router.use("/contact", ((contactR) => {
    contactR.get('/', Contact.getAll);
    contactR.post('/', Contact.register);
    contactR.put('/:id', Contact.edit);
    contactR.delete('/:id', Contact.remove);
    return contactR;
})(express.Router()));

router.use("/countries", ((countryR) => {
    countryR.get('/', Country.getAll);
    countryR.post('/', Country.register);
    countryR.put('/:id', Country.edit);
    countryR.delete('/:id', Country.remove);
    return countryR;
})(express.Router()));

router.use("/jobs", ((jobR) => {
    jobR.get('/', Jobs.getAll);
    jobR.post('/', Jobs.register);
    jobR.get('/:id', Jobs.getOne);
    jobR.put('/:id', Jobs.edit);
    jobR.delete('/:id', Jobs.remove);
    return jobR;
})(express.Router()));

router.use("/salaries", ((salaryR) => {
    salaryR.get('/', Salary.getAll);
    salaryR.post('/', Salary.register);
    salaryR.put('/:id', Salary.edit);
    salaryR.delete('/:id', Salary.remove);
    return salaryR;
})(express.Router()));

router.use("/status", ((statusR) => {
    statusR.get('/', Status.getAll);
    statusR.post('/', Status.register);
    statusR.put('/:id', Status.edit);
    statusR.delete('/:id', Status.remove);
    return statusR;
})(express.Router()));

router.use("/statistics", ((statisticsR) => {
    statisticsR.get('/workcity', Statistics.getWorkCity);
    statisticsR.get('/workcountry', Statistics.getWorkCountry);
    statisticsR.get('/worksalary', Statistics.getWorkSalary);
    statisticsR.get('/userstatus', Statistics.getUserStatus);
    statisticsR.get('/salaryavg', Statistics.getSalaryAvg);
    statisticsR.get('/companyjob', Statistics.getCompanyJob);
    return statisticsR;
})(express.Router()));
*/

export default router;