import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';

import { Product } from '../item/item.component';

let users = JSON.parse(localStorage.getItem('users')) || [];

let prod: Product[] = [
    // { id : 1, name : "Fejhallgato", description : "Ez 1 nagyon hosszú leirás", categoryid : 10, price : 99999, discountid : null },
    // { id : 2, name : "Mikrofon", description : "Ez 2 nagyon hosszú leirás", categoryid : 5, price : 5499, discountid : null },
    // { id : 3, name : "Mikrofon ketto", description : "Ez 3 nagyon hosszú leirás", categoryid : 2, price : 19999, discountid : null },
    // { id : 4, name : "Asztal", description : "Ez 4 nagyon hosszú leirás", categoryid : 6, price : 57999, discountid : null },
    // { id : 5, name : "Szék", description : "Ez 5 nagyon hosszú leirás", categoryid : 13, price : 1199, discountid : null }/*
    // { id : 5, name : "Szék", description : "Ez 5 nagyon hosszú leirás", categoryid : 13, price : 1199, discountid : null },
    // { id : 5, name : "Szék", description : "Ez 5 nagyon hosszú leirás", categoryid : 13, price : 1199, discountid : null },
    // { id : 5, name : "Szék", description : "Ez 5 nagyon hosszú leirás", categoryid : 13, price : 1199, discountid : null },
    // { id : 5, name : "Szék", description : "Ez 5 nagyon hosszú leirás", categoryid : 13, price : 1199, discountid : null },
    // { id : 5, name : "Szék", description : "Ez 5 nagyon hosszú leirás", categoryid : 13, price : 1199, discountid : null },
    // { id : 5, name : "Szék", description : "Ez 5 nagyon hosszú leirás", categoryid : 13, price : 1199, discountid : null },
    // { id : 5, name : "Szék", description : "Ez 5 nagyon hosszú leirás", categoryid : 13, price : 1199, discountid : null },
    // { id : 5, name : "Szék", description : "Ez 5 nagyon hosszú leirás", categoryid : 13, price : 1199, discountid : null },
    // { id : 5, name : "Szék", description : "Ez 5 nagyon hosszú leirás", categoryid : 13, price : 1199, discountid : null },
    // { id : 5, name : "Szék", description : "Ez 5 nagyon hosszú leirás", categoryid : 13, price : 1199, discountid : null },
    // { id : 5, name : "Szék", description : "Ez 5 nagyon hosszú leirás", categoryid : 13, price : 1199, discountid : null },
    // { id : 5, name : "Szék", description : "Ez 5 nagyon hosszú leirás", categoryid : 13, price : 1199, discountid : null },
    // { id : 5, name : "Szék", description : "Ez 5 nagyon hosszú leirás", categoryid : 13, price : 1199, discountid : null }*/
  ];

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, body } = request;

        // wrap in delayed observable to simulate server api call
        return of(null)
            .pipe(mergeMap(handleRoute))
            .pipe(materialize()) // call materialize and dematerialize to ensure delay even if an error is thrown 
            .pipe(delay(500))
            .pipe(dematerialize());

        function handleRoute() {
            switch (true) {
                //  case url.endsWith('/users/authenticate') && method === 'POST':
                //      return authenticate();
                // case url.endsWith('/products') && method === 'GET':
                //     return GetProducts();
                // case url.endsWith('\/product\/\d+$') && method === 'GET':
                //     return getProductById();
                // case url.endsWith('/users') && method === 'GET':
                //     return getUsers();
                // case url.endsWith('/jobs') && method === 'GET':
                //     return getJobs();
                // case url.match(/\/users\/\d+$/) && method === 'DELETE':
                //     return deleteUser();
                default:
                    // pass through any requests not handled above
                    return next.handle(request);
            }    
        }

        // route functions

        function authenticate() {
            const { username, password } = body;
            const user = users.find(x => x.username === username && x.password === password);
            if (!user) return error('Username or password is incorrect');
            return ok({
                id: user.id,
                username: user.username,
                email: user.email,
                token: 'fake-jwt-token'
            })
        }

        function GetProducts() {
            return ok(prod);
        }

        function getProductById(){
            console.log("GETPRODUCTBYID");
            let lastProduct = prod.pop();
            return ok(lastProduct);
        }
        
        // helper functions

        function ok(body?) {
            return of(new HttpResponse({ status: 200, body }))
        }

        function error(message) {
            return throwError({ error: { message } });
        }

        function unauthorized() {
            return throwError({ status: 401, error: { message: 'Unauthorised' } });
        }

        function isLoggedIn() {
            return headers.get('Authorization') === 'Bearer fake-jwt-token';
        }

        function idFromUrl() {
            const urlParts = url.split('/');
            return parseInt(urlParts[urlParts.length - 1]);
        }
    }
}

export const fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};

