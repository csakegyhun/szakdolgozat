import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as config from '../environment';
import { Product } from '../item/item.component';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

constructor(
  private http: HttpClient) { }

  GetAllProduct(){
    return this.http.get<any[]>(`${config.backendURL}/products`).toPromise();
  }

  GetProductById(productId: number){
    return this.http.get<Product>(`${config.backendURL}/product/${productId}`);
  }

  edit(product) {
    return this.http.put(`${config.backendURL}/product/${product.id}`, product).toPromise();
  }

  create(product) {
    return this.http.post(`${config.backendURL}/product/`, product).toPromise();
  }


}
