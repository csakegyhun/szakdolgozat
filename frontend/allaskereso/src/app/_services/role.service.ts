import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


import * as config from '../environment';
import { Product } from '../item/item.component';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(
    private http: HttpClient) { }
  
    GetAllRole(){
      return this.http.get<any[]>(`${config.backendURL}/roles`).toPromise();
    }

}
