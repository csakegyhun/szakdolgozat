import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import * as config from '../environment';


@Injectable({ providedIn: 'root' })
export class StatusService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<any[]>(`${config.backendURL}/status`);
    }

    register(status) {
        return this.http.post(`${config.backendURL}/status`, status);
    }

    modify(status) {
        return this.http.put(`${config.backendURL}/status/${status.id}`, status);
    }

    delete(id) {
        return this.http.delete(`${config.backendURL}/status/${id}`);
    }
}