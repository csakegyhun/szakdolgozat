import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import * as config from '../environment';


@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<any[]>(`${config.backendURL}/users`);
    }

    register(user) {
        return this.http.post(`${config.backendURL}/users`, user);
    }

    modify(user) {
        return this.http.put(`${config.backendURL}/users/${user.id}`, user);
    }

    delete(id) {
        return this.http.delete(`${config.backendURL}/users/${id}`);
    }
}