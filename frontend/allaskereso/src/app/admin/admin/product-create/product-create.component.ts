import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/item/item.component';
import { ProductService } from 'src/app/_services';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {

  products: Product[];
  error: string;
  success: string;
  selectedProduct: Product;

  async ngOnInit() {
    this.success = "";
    this.error = "";
    this.selectedProduct ={
      id: null,
      name: "",
      price: 0,
      description: "",
      imagePath: "",
      discountid: null,
      categoryid: null
    }
  }

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private router: Router
  ) {
   }

  createProduct() {
    
    if( 
      this.selectedProduct.name != "" &&
      this.selectedProduct.price != 0 &&
      this.selectedProduct.description != "" &&
      this.selectedProduct.imagePath != "" 
     ){
        let temp = this.selectedProduct.imagePath;
        this.selectedProduct.imagePath = "assets/images/"+temp+".png";
        this.productService.create(this.selectedProduct).then(data =>{
          this.success = "Sikeres termék létrehozás";      
        });
      }
  }
}
