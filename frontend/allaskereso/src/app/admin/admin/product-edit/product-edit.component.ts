import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { element } from 'protractor';
import {Observable} from 'rxjs';
import {first, map, startWith} from 'rxjs/operators';
import { Product } from 'src/app/item/item.component';
import { ProductService } from 'src/app/_services';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  myControl = new FormControl();
  filteredOptions: Observable<string[]>;
  products: Product[];
  options: string[] = [];
  hasValue: boolean = false;
  error: string;
  success: string;

  selectedProduct: Product;

  async ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );
    this.loadProducts().then(() => {
    }).catch((error) =>{
      console.log(error);
    })
    this.success = "";
    this.error = "";
  }

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private router: Router
  ) {
   }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  loadProducts() {
      return this.productService.GetAllProduct().then(data =>{
        this.products = data;
        this.products.forEach(elem => this.options.push(elem.name));
        this.hasValue = true;
        this.selectedProduct = this.products[0];
      });
  }

  selectedItemChanged(event){
    this.selectedProduct = this.products.filter(prod => prod.name == event.option.value).pop();
  }

  saveProduct() {
    let temp = this.selectedProduct.imagePath;
    this.selectedProduct.imagePath = "assets/images/"+temp+".png";
    this.productService.edit(this.selectedProduct).then(data =>{
          this.success = "Sikeres termék módisítás";      
    });
}

}
