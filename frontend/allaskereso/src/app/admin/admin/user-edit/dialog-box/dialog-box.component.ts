import { Component, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StatusService } from 'src/app/_services';
import { first } from 'rxjs/operators';
import { User } from '../user-edit.component';
import { RoleService } from 'src/app/_services/role.service';


@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.css']
})
export class DialogBoxComponent {

  action: string;
  local_data: any;
  roles: any;

  constructor(
    public dialogRef: MatDialogRef<DialogBoxComponent>,
    private statusService: StatusService,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: User,
    private roleService: RoleService) {
    this.loadRoles();
    this.local_data = { ...data };
    this.action = this.local_data.action;

  }

  doAction() {
    this.dialogRef.close({ event: this.action, data: this.local_data });
  }

  closeDialog() {
    this.dialogRef.close({ event: 'Cancel' });
  }

  loadRoles() {
    this.roleService.GetAllRole().then(data =>{
      this.roles = data;
    });
  }

}