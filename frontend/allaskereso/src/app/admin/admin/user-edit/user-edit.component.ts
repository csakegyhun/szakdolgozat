import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { first } from 'rxjs/operators';
import { UserService } from '../../../_services';
import { MatSort } from '@angular/material/sort';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatTable } from '@angular/material/table';
import { DialogBoxComponent } from './dialog-box/dialog-box.component';
import { MatDialog } from '@angular/material/dialog';

export interface User {
  id: number;
  username: string;
  password: string;
  email: string;
  registeredat: Date;
  role: number;
}

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  insertForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    public dialog: MatDialog
  ) {

  }

  async ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    await this.loadAllUsers();

    this.insertForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', [Validators.required]]
    });
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatTable, { static: true }) table: MatTable<User>;

  displayedColumns: string[] = ['id', 'username', 'email', 'password', 'registeredat', 'role', 'actions'];
  dataSource = new MatTableDataSource<User>([]);
  selection = new SelectionModel<User>(true, []);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  private loadAllUsers() {
    this.userService.getAll()
      .pipe(first())
      .subscribe(users => {
        this.dataSource.data = users;
        console.log(users);
      });
  }

  openDialog(action, obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data: obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Add') {
        this.addRowData(result.data);
      } else if (result.event == 'Update') {
        this.updateRowData(result.data);
      } else if (result.event == 'Delete') {
        this.deleteRowData(result.data);
      }
    });
  }

  addRowData(row_obj) {
    this.dataSource.data.push({
      id: this.dataSource.data.length + 1,
      username: row_obj.username,
      email: row_obj.email,
      password: row_obj.password,
      registeredat: row_obj.registeredat,
      role: row_obj.role
    });
    this.userService.register(row_obj)
      .pipe(first())
      .subscribe();
    this.dataSource.data = this.dataSource.data.filter((value, key) => {
      return true;
    });
  }

  updateRowData(row_obj) {
    this.dataSource.data = this.dataSource.data.filter((value, key) => {
      if (value.id == row_obj.id) {
        value.username = row_obj.username;
        value.email = row_obj.email;
        value.password = row_obj.password;
        value.registeredat = row_obj.registeredat,
        value.role = row_obj.role
      }
      this.userService.modify(row_obj)
      .pipe(first())
      .subscribe();
      return true;
    });
  }

  deleteRowData(row_obj) {
    this.userService.delete(row_obj.id)
      .pipe(first())
      .subscribe();
    this.dataSource.data = this.dataSource.data.filter((value, key) => {
      return value.id != row_obj.id;
    });
  }

}
