import { Component } from '@angular/core';
import { first } from 'rxjs/operators';
import { AuthenticationService } from './_services';
import { Router } from '@angular/router';
import { CartService } from './_services/cart.service';
import { Product } from './item/item.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Allaskereso';

  openSidenav = false;

  currentUser: any;
  show=true;
  isLoggedIn : boolean;
  isAdmin: boolean = false;

  cartItems: any;
  cartSumPrice: number = 0;
  cartItemLength: number = 0;

  constructor(
      private router: Router,
      private authenticationService: AuthenticationService,
      private cartService: CartService
  ) {
      this.currentUser = this.authenticationService.currentUserValue;
      if (this.currentUser == null){
        this.isLoggedIn = false;
      }else{
        this.isLoggedIn = true;
      }
  
  }

  ngOnInit() {
    this.cartItems = this.cartService.getItems();
    let sum = 0;
    let sumCount = 0;
    this.cartItems.forEach(element => {
       sum += element.product.price * element.quantity; 
       sumCount += element.quantity;
      });
    this.cartSumPrice = sum;
    this.cartItemLength = sumCount;
    const currentUser = this.authenticationService.currentUserValue;
    if (currentUser.role.name == "Admin") {
      this.isAdmin = true;
    }
  }

  logout() {
    this.authenticationService.logout();
    localStorage.removeItem("cart");
    this.router.navigate(['/']);
    window.location.reload();
  }


}
