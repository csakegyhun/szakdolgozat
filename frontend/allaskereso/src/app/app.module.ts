import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend
import { fakeBackendProvider } from './_helpers';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';


import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { MaterialModule } from './material.module';
import { SearchfieldComponent } from './searchfield/searchfield.component';
import { FooterComponent } from './footer/footer.component'
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component'
import { AuthGuard } from './_helpers';
import { AlertComponent } from './alert/alert.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { ItemComponent } from './item/item.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';
import { CartComponent } from './cart/cart.component';
import { AdminComponent } from './admin/admin/admin.component';
import { UserEditComponent } from './admin/admin/user-edit/user-edit.component';
import { DialogBoxComponent } from './admin/admin/user-edit/dialog-box/dialog-box.component';
import { ProductEditComponent } from './admin/admin/product-edit/product-edit.component';
import { ProductCreateComponent } from './admin/admin/product-create/product-create.component';


const appRoutes: Routes = [
  { path: '', component: MainpageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'cart', component: CartComponent},
  { path: 'admin', canActivate: [AuthGuard],
    component: AdminComponent,
    children: [
      { path: 'users', component: UserEditComponent, outlet: 'adminpage'},
      { path: 'product-edit', component: ProductEditComponent, outlet: 'adminpage'},
      { path: 'product-create', component: ProductCreateComponent, outlet: 'adminpage'},
    ]
},
  
  { path: 'product/:productId', component: ItemDetailComponent},
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  declarations: [	
    AppComponent,
    ToolbarComponent,
    SearchfieldComponent,
    FooterComponent,
    RegistrationComponent,
    LoginComponent,
    AlertComponent,
    MainpageComponent,
    ItemComponent,
    ItemDetailComponent,
    CartComponent,
    AdminComponent,
    UserEditComponent,
    DialogBoxComponent,
    ProductEditComponent,
    ProductCreateComponent
   ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true, relativeLinkResolution: 'legacy' }
    )
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    // provider used to create fake backend
    fakeBackendProvider,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
