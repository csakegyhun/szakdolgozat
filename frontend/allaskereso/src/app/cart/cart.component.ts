import { Component, OnInit } from '@angular/core';
import { Product } from '../item/item.component';
import { CartService } from '../_services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cartProducts: Product[];
  showDataNotFound = true;

  constructor(
    private cartService: CartService
  ) { }

  ngOnInit() {
    this.getCartProduct();
  }

  removeCartProduct(product: Product) {
    this.cartService.deleteItem(product);
    this.getCartProduct();
  }

  getCartProduct() {
    this.cartProducts = this.cartService.getItems();
  }

}
