import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { Product } from '../item/item.component';
import { ProductService } from '../_services';
import { CartService } from '../_services/cart.service';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {

  // selectedProduct: Product = {
  //   id : 1,
  //   name : "Fejhallgato",
  //   description : "Ez 1 nagyon hosszú leirás",
  //   price : 99999,
  //   categoryid : 10,
  //   discountid : null ,
  //   imagePath: "assets/images/product_image.png"
  // }

  selectedProduct: Product;
  private routeSub: Subscription;


  constructor(
    private cartService: CartService,
    private productService: ProductService,
    private route: ActivatedRoute,
    private router: Router

  ) { }

  async ngOnInit() {
    await this.loadProduct();
  }

  async loadProduct(){
    this.routeSub =  await this.route.params.subscribe(async params => {
      await this.productService.GetProductById(params['productId'])
        .pipe(first())
        .subscribe(product => {
          this.selectedProduct = product;
        });
      });
  
  }

  addToCart(product: Product){
    this.cartService.addToCart(product);
  }

}
