import { Component, OnInit, Input } from '@angular/core';
import { provideRoutes } from '@angular/router';
import { first } from 'rxjs/operators';
import { CartService } from '../_services/cart.service';
import { ProductService } from '../_services/product.service';

export interface Product{
  id: number;
  name: string;
  description: string;
  categoryid: number;
  price: number;
  discountid: number;
  imagePath: string;
}

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})

export class ItemComponent implements OnInit {

  prod :Product[];
  showedProd = null;

  public FilterProducts(filteredData){
    this.showedProd = filteredData;
  }

  public TurnFilterOff(){
    this.showedProd = this.prod;
  }
  
  constructor(
    private productService: ProductService,
    private cartService: CartService,
  ) {
   }

  ngOnInit(): void {
    this.loadProducts();
  }

  public onCardClick(evt: MouseEvent){
    console.log(evt);
  }

  private loadProducts() {
    this.productService.GetAllProduct()
        .then(data => {
          this.prod = data;
          this.showedProd = data
        });
  }

  addToCart(product: Product){
    this.cartService.addToCart(product);
  }

}
