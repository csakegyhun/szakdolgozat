import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from '../item/item.component';

@Component({
  selector: 'app-searchfield',
  templateUrl: './searchfield.component.html',
  styleUrls: ['./searchfield.component.css']
})
export class SearchfieldComponent implements OnInit {

  @Input() data: Product[];
  @Output() filterData: EventEmitter<Product[]> = new EventEmitter();

  public filter(filterBy: string){

    var split = filterBy.split(" ");

    var newData = new Array<Product>();

    split.forEach(substring => {

      this.data.forEach(d =>{

        if( d.name.toLowerCase().includes(substring.toLowerCase()) )
        {
          if( newData.indexOf(d) === -1 )
          {
            newData.push(d);
          }
        }
      });

    });

    this.filterData.emit(newData);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
