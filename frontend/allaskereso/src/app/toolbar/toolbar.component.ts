import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Router } from '@angular/router';
import { AuthenticationService } from '../_services';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  @Output() toggleEmitter = new EventEmitter<void>();

  currentUser: any;
  isLogged: boolean

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    if (this.currentUser == null){
      this.isLogged = false;
    }else{
      this.isLogged = true;
    }
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/']);
    window.location.reload();
  }

  ngOnInit() {
  }

  sidenavToggle() {
    this.toggleEmitter.emit();
  }

}
